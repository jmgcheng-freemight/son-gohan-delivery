angular.module('deliveryAppControllers', ['ionic', 'ionic.service.core', 'ngCordova', 'ngCordovaOauth', 'ngSanitize'])

.controller('mainCtrl', ["$rootScope", "$scope", "$http", "$cordovaOauth", "$ionicLoading", "$location", "$state", "$ionicSideMenuDelegate", "$cordovaGeolocation", "$ionicPlatform", "$cordovaNetwork", "$ionicPopup", "$ionicSlideBoxDelegate", "$timeout",
	function($rootScope, $scope, $http, $cordovaOauth, $ionicLoading, $location, $state, $ionicSideMenuDelegate, $cordovaGeolocation, $ionicPlatform, $cordovaNetwork, $ionicPopup, $ionicSlideBoxDelegate, $timeout ) {
	
	/*
		init
	*/
	$scope.init = function(){
		//
		$ionicLoading.show({template: 'Please Wait..'});
		
		//
		$scope.s_fb_app_api 				= "1699062197038796";
		$scope.s_site_app_api 				= "1010deliverybaseapp1010";
		$scope.s_site_app_menu_page_url 	= "http://freemight.com/demo/son-gohan/bda-menu-page-app/";
		$scope.s_site_app_order_page_url 	= "http://freemight.com/demo/son-gohan/bda-order-page-app";
		//$scope.s_site_app_order_page_url 	= "http://localhost/wp-delivery-base-app/bda-order-page-app";
				
		//setPlatformDetail
		console.log('setPlatformDetail');
		$scope.setPlatformDetail();
		console.log( $scope.o_platform );
		
		//resetCustomerDetail
		console.log('resetCustomerDetail');
		$scope.resetCustomerDetail();
		console.log($scope.o_customer);
		
		//init o_cart
		console.log('init o_cart');
		$scope.o_cart = {};
		console.log($scope.o_cart);
		
		//init i_totalOrderQty
		console.log('init i_totalOrderQty');
		$scope.i_totalOrderQty = 0;
		console.log($scope.i_totalOrderQty);
		
		//init config.txt o_site_config
		console.log('init config.txt');
		$scope.o_site_config = {};
		var s_menu_url = "json-data/config.txt";
		$http.get(s_menu_url).success( function(o_response) {
			$scope.o_site_config = o_response; 
			console.log( $scope.o_site_config );
		});
		
		//
		$ionicLoading.hide();
		
		//
		if( $scope.b_isOnline )
		{
			//cordovaGeolocation
			console.log('Try cordovaGeolocation');
			$scope.setCustomerGeolocation();
			
			//init menu. Get menu from website. Site returns array of object
			console.log('init menu');
			$scope.a_restaurant_menu_ids = [];
			$scope.a_restaurant_menu = [];
			var s_menu_url = $scope.s_site_app_menu_page_url;
			//var s_menu_url = "json-data/menu.txt";
			var o_params = {
				's_site_order_app_api'	:$scope.s_site_app_api
			};
			$ionicLoading.show({template: 'Please Wait..'});
			$http({
				method		:'POST',
				url			:s_menu_url,
				data		:$.param(o_params),
				headers		: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function successCallback(response) {
				$scope.o_restaurant_menus = response.data.a_menu; 
				//$scope.o_restaurant_menus = response.data; 
				console.log( $scope.o_restaurant_menus );
				// store in array for convenient use later
				for( i_counter=0; i_counter<$scope.o_restaurant_menus.length; i_counter++)
				{
					$scope.a_restaurant_menu_ids.push( $scope.o_restaurant_menus[i_counter]['i_id'] );
					$scope.a_restaurant_menu[$scope.o_restaurant_menus[i_counter]['i_id']] = $scope.o_restaurant_menus[i_counter];
				}
				console.log( $scope.a_restaurant_menu_ids );
				$ionicLoading.hide();
				/*
				console.log('$ionicSlideBoxDelegate');
				console.log($ionicSlideBoxDelegate);
				setTimeout(function(){ 
					$ionicSlideBoxDelegate.next(1);
				}, 5000);
				*/
			}, function errorCallback(response) {
				console.log(response);
				//alert('Error Menu');
				$ionicPopup.alert({
					title: 'Notice',
					template: "Menu Server Not Available"
				});
				$ionicLoading.hide();
			});
		}
		else
		{
			$scope.setOfflineNotice();
		}

		
		
	};
	
	
	/*
	*/
	$scope.ionicUserSetup = function(){
		//
		var o_ionicUserAuthDetails = {
		//'email': 'jmgcheng@yahoo.com',
		'email'		: $scope.o_customer.s_email,
		'password': 'secretpassword' + $scope.o_customer.s_email
		};
		//
		var o_ionicUserAuthOptions = { 'remember': true };
		Ionic.Auth.signup(o_ionicUserAuthDetails).then(
			function(){
				//alert('ionicRegister success');
				$scope.ionicLogin();
			}, 
			function(){
				//alert('ionicRegister fail');
				$scope.ionicLogin();
			}
		);
	};
	
	
	/*
	*/
	$scope.ionicLogin = function(){
		//
		var o_ionicUserAuthDetails = {
		//'email': 'jmgcheng@yahoo.com',
		'email'		: $scope.o_customer.s_email,
		'password': 'secretpassword' + $scope.o_customer.s_email
		};
		var o_ionicUserAuthOptions = { 'remember': true };
		//
		Ionic.Auth.login('basic', o_ionicUserAuthOptions, o_ionicUserAuthDetails).then(function(){
			var user = Ionic.User.current();
			if (user.isAuthenticated()) 
			{
				//alert('isAuthenticated success');
				var push = new Ionic.Push({
					"debug": true
				});
				push.register(function(token) {
					//alert(token.token);
					console.log("Device token:",token.token);
					push.saveToken(token);  // persist the token in the Ionic Platform
				});
				//alert('push.register below');
			} 
			else 
			{
			  //alert('isAuthenticated fail');
			  setTimeout($scope.ionicUserSetup(), 120000);
			}
		}, function(){
			//alert('login fail');
			setTimeout($scope.ionicUserSetup(), 120000);
		});
	};
	
	
	/*
	*/
	$scope.login = function(){
		console.log('trying to login');
		if( $scope.o_platform.platform == 'android' )
		{
			//
			$ionicLoading.show({template: 'Please Wait..'});
			//
			$cordovaOauth.facebook($scope.s_fb_app_api, ["email", "public_profile"], {redirect_uri: "http://localhost/callback"}).then(function(result){
				//
				//$scope.o_customer.s_fbAccessToken = result.access_token;
				//
				$http
					.get(
						"https://graph.facebook.com/v2.2/me", 
						{
							params: {
								access_token: result.access_token, 
								fields: "name,picture,email", 
								format: "json" 
							}
						}
					)
					.then(function(result) {
						//$scope.customerData = result.data;
						//$scope.customerPic = result.data.picture.data.url;
						$scope.o_customer.b_login = true;
						$scope.o_customer.s_fullName = result.data.name;
						$scope.o_customer.s_email = result.data.email;
						$ionicLoading.hide();
						//$state.go('app.slide-menu');
						$ionicSideMenuDelegate.toggleLeft(false);
						//
						$scope.ionicUserSetup();
					}, function(error) {
						//alert("Error: " + error);
						$ionicPopup.alert({
							title: 'Notice',
							template: "Error: " + error
						});
						$ionicLoading.hide();
					});

			}, function(error){
					//alert("Error: " + error);
					$ionicPopup.alert({
						title: 'Notice',
						template: "Error: " + error
					});
					$ionicLoading.hide();
			});			
		}
		else
		{
			//alert('Platform Login NOT Supported');
			$ionicPopup.alert({
				title: 'Notice',
				template: 'Platform Login NOT Supported'
			});
			//$state.go('app.slide-menu');
			//$ionicSideMenuDelegate.toggleLeft(true);
		}
	};
	
	
	/*
	*/
	$scope.logout = function(){
		$ionicLoading.show({template: 'Please Wait..'});
		$scope.resetCustomerDetail();
		//$state.go('app.top');
		$ionicSideMenuDelegate.toggleLeft(false);
		$ionicLoading.hide();
	};
	
	
	/*
	*/
	$scope.submitOrder = function(frm_order){
		console.log('submitOrder');
		var b_emailSent = false;
		var d = new Date();
		var s_timeStamp = d.getTime();
		//
		var o_params = {
			's_timeStamp'			:s_timeStamp,
			's_date'				:$scope.getDate(),
			's_cellphoneNumber'		:$scope.o_customer.s_cellphoneNumber,
			's_deliveryAddress'		:$scope.o_customer.s_deliveryAddress,
			's_orderNote'			:$scope.o_customer.s_orderNote,
			'o_customer'			:$scope.o_customer,
			'o_cart'				:$scope.o_cart,
			's_site_order_app_api'	:$scope.s_site_app_api
		};
		console.log( o_params );
		$ionicLoading.show({template: 'Please Wait..'});
		$http({
			method	:'POST',
			url		:$scope.s_site_app_order_page_url,
			data	:$.param(o_params),
			headers	:{'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function successCallback(response) {
			console.log(response);
			//alert(response.data.s_message);
			$ionicPopup.alert({
				title: 'Notice',
				template: response.data.s_message
			});
			$ionicLoading.hide();
			$state.go('app.checkout-success');
		}, function errorCallback(response) {
			console.log(response);
			$ionicPopup.alert({
				title: 'Notice',
				template: 'Error'
			});
			$ionicLoading.hide();
		});
	};
	
	
	/*
	*/
	$scope.getDate = function() {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
		dd='0'+dd
		} 
		if(mm<10) {
		mm='0'+mm
		} 
		today = mm+'/'+dd+'/'+yyyy;
		return today;
	};
	
	
	/*
	*/
	$scope.setPlatformDetail = function(){
		$scope.o_platform = {};
		$scope.o_platform.isWebView = ionic.Platform.isWebView();
		$scope.o_platform.isIPad = ionic.Platform.isIPad();
		$scope.o_platform.isIOS = ionic.Platform.isIOS();
		$scope.o_platform.isAndroid = ionic.Platform.isAndroid();
		$scope.o_platform.isWindowsPhone = ionic.Platform.isWindowsPhone();
		$scope.o_platform.platform = ionic.Platform.platform();
	};
	
	
	/*
	*/
	$scope.resetCustomerDetail = function(){
		$scope.o_customer = {};
		$scope.o_customer = {
			'b_login'			:false,
			's_fbAccessToken'	:'',
			's_fullName'		:'',
			's_email'			:'',
			's_cellphoneNumber'	:'',
			's_deliveryAddress'	:'',
			's_orderNote'		:'',
			'd_cashOnHand'		:'',
			'd_expectedChange'	:'',
			's_gpsLat'			:'',
			's_gpsLong'			:'',
			'b_online'			:''//$scope.isOnline()
		};
	};
	
	
	/*
	*/
	$scope.addQtyMenuOrder = function(i_menu_id){
		// check if menu id does exist
		if( $scope.a_restaurant_menu_ids.indexOf(i_menu_id) != -1 )
		{
			var i_qty = 0;
			// if undefined == first time putting this menu in cart
			if(  $scope.o_cart[i_menu_id] == undefined )
			{
				i_qty = 1;
				$scope.o_cart[i_menu_id] = {
					'i_menuId' : i_menu_id,
					'i_qty' : i_qty,
					'd_subTotal' : i_qty * $scope.a_restaurant_menu[i_menu_id].d_price,
					'o_menuDetails' : $scope.a_restaurant_menu[i_menu_id]
				};
			}
			else
			{
				i_qty = $scope.o_cart[i_menu_id].i_qty + 1;
				$scope.o_cart[i_menu_id] = {
					'i_menuId' : i_menu_id,
					'i_qty' : i_qty,
					'd_subTotal' : i_qty * $scope.a_restaurant_menu[i_menu_id].d_price,
					'o_menuDetails' : $scope.a_restaurant_menu[i_menu_id]
				};
			}
		}
		else
		{ 
			$ionicPopup.alert({
				title: 'Notice',
				template: 'Menu ID not found'
			});
		}
		//
		$scope.i_totalOrderQty = $scope.getTotalOrderQty();
		$scope.updateCustomerChange();
		//
		var s_cart_detail_temp = JSON.stringify( $scope.o_cart );
		$scope.o_cart = JSON.parse( s_cart_detail_temp );
		//console.log( $scope.i_totalOrderQty );
		
		console.log( $scope.o_cart );
	};
	
	
	/*
	*/
	$scope.deductQtyMenuOrder = function(i_menu_id){
		// check if menu id does exist
		if( $scope.a_restaurant_menu_ids.indexOf(i_menu_id) != -1 )
		{
			var i_qty = 0;
			if( $scope.o_cart[i_menu_id] == undefined )
			{
				//no need to deduct item that is undefined inside our cart
			}
			else
			{
				i_qty = $scope.o_cart[i_menu_id].i_qty - 1;
				if( i_qty < 0 )
				{
					i_qty = 0;
				}
				else
				{
					$scope.o_cart[i_menu_id].i_qty = i_qty;
					$scope.o_cart[i_menu_id].d_subTotal = i_qty * $scope.a_restaurant_menu[i_menu_id].d_price;
				}
				// delete order if qty 0
				if( i_qty == 0 )
				{
					//$scope.o_cart[i_menu_id] = undefined;
					delete $scope.o_cart[i_menu_id];
				}
			}
		}
		else
		{
			$ionicPopup.alert({
				title: 'Notice',
				template: 'Menu ID not found'
			});
		}
		//
		$scope.i_totalOrderQty = $scope.getTotalOrderQty();
		$scope.updateCustomerChange();
		//
		var s_cart_detail_temp = JSON.stringify( $scope.o_cart );
		$scope.o_cart = JSON.parse( s_cart_detail_temp );
		//console.log( $scope.i_totalOrderQty );
		
		console.log( $scope.o_cart );
	};
	
	
	/*
	*/
	$scope.removeMenuOrder = function(i_menu_id){
		delete $scope.o_cart[i_menu_id];
		$scope.i_totalOrderQty = $scope.getTotalOrderQty();
		$scope.updateCustomerChange();
	};
	
	
	/*
	*/
	$scope.getTotalOrderQty = function(){
		var i_qty = 0;
		for( var o_item in $scope.o_cart ) 
		{
			if( $scope.o_cart.hasOwnProperty(o_item) ) 
			{
				i_qty = i_qty + $scope.o_cart[o_item].i_qty;
			}
		}
		return i_qty;
	};
	
	
	/*
	*/
	$scope.getTotalOrderCost = function (){
		var i_cost = 0;
		for( var o_item in $scope.o_cart ) 
		{
			if( $scope.o_cart.hasOwnProperty(o_item) ) 
			{
				i_cost = i_cost + $scope.o_cart[o_item].d_subTotal;
			}
		}
		return i_cost;
	};
	
	
	/*
	*/
	$scope.updateCustomerChange = function (){
		$scope.o_customer.d_expectedChange = $scope.o_customer.d_cashOnHand - $scope.getTotalOrderCost();
	};
		
	
	/*
	*/
	$scope.options = {
		scrollbarDraggable: true,
		//setWrapperSize: true,
		//autoHeight: true,
		//height: '400px'
	};
	$scope.data = {};
	$scope.$watch('data.slider', function(nv, ov) {
		$scope.slider = $scope.data.slider;
	});
	
	
	
	
	/*
	*/
	$scope.setCustomerGeolocation = function(){
		//
		var o_posOptions = { 
							timeout 			:10000, 
							enableHighAccuracy	:true
		};
		$cordovaGeolocation
			.getCurrentPosition(o_posOptions)
			.then(function (o_position) {
				console.log( o_position );
				$scope.o_customer.s_gpsLat	= o_position.coords.latitude;
				$scope.o_customer.s_gpsLong	= o_position.coords.longitude;
				//console.log( $scope.o_customer );
				//
				console.log('setCustomerAddressGMAPI');
				$scope.setCustomerAddressGMAPI( $scope.o_customer.s_gpsLat, $scope.o_customer.s_gpsLong);
			}, 
			function(error) {
				console.log( error );
				$ionicPopup.alert({
					title: 'Notice',
					template: 'Unable to get location using GPS'
				});
			});	
	};
	
	
	/*
		set customer address using google map api
	*/
	$scope.setCustomerAddressGMAPI = function(s_gpsLat, s_gpsLong){
		if( s_gpsLat != '' && s_gpsLat != '' )
		{
			var s_latlng = s_gpsLat + ',' + s_gpsLong;
			$http({
				method		:'GET',
				url			:'http://maps.googleapis.com/maps/api/geocode/json',
				params		:{
								'latlng':s_latlng
							}
			}).then(function successCallback(response) {
				console.log('setCustomerAddressGMAPI - GMAPI response');
				console.log( response );
				//
				$scope.o_customer.s_deliveryAddress = ( response.data.results[0].formatted_address != undefined ) ? response.data.results[0].formatted_address : '';
				console.log( $scope.o_customer );
			}, function errorCallback(response) {
				console.log('setCustomerAddressGMAPI - GMAPI response ERROR');
				console.log(response);
			});
		}
		else
		{
			console.log('Invalid Google Map LatLng');
			$ionicPopup.alert({
				title: 'Notice',
				template: 'Invalid Google Map LatLng'
			});
		}
	};
	
	
	/*
	*/
	$scope.setOfflineNotice = function(){
		$ionicLoading.hide();
		$ionicLoading.show({template: 'Please connect to the Internet..'});
	};
	
	
	/*
	*/
	$scope.setOnlineNotice = function(){
		$ionicLoading.hide();
		$ionicLoading.show({template: 'Connected, please wait.'});
		$ionicLoading.hide();
	};
	
	
	/*
	*/
	$ionicPlatform.on("online", function(){
		//alert(navigator.onLine);
		$scope.b_isOnline = true;
		$scope.setOnlineNotice();
		$scope.init();
	}, false);
	
	
	/*
	*/
	$ionicPlatform.on("offline", function(){
		//alert(navigator.onLine);
		$scope.b_isOnline = false;
		$scope.setOfflineNotice();
	}, false);
	
	
	
	/*
		run init ONLY if we have internet
	*/
	$scope.b_isOnline = false;
	ionic.Platform.ready(function(){
		//
		if( ionic.Platform.isWebView() ) //running on cordova
		{
			$scope.b_isOnline = $cordovaNetwork.isOnline();
		}
		else //should be desktop
		{
			$scope.b_isOnline = navigator.onLine;
		}
		//
		if( $scope.b_isOnline )
		{
			$scope.setOnlineNotice();
			$scope.init();
		}
		else
		{
			$scope.setOfflineNotice();
		}
	});
	  
}])

.filter('customCurrency', ["$filter", function ($filter) {       
    return function(amount, currencySymbol){
        var currency = $filter('currency');         

        if(amount < 0){
            return currency(amount, currencySymbol).replace("(", "-").replace(")", ""); 
        }

        return currency(amount, currencySymbol);
    };
}])


.directive('bda_expectedChange', function($q, $timeout) {
	return {
		//require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
			
			console.log(scope);
			
		}
	};
});
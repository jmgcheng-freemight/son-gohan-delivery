// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('deliveryApp', ['ionic','ionic.service.core', 'deliveryAppControllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('app', {
			url: '/app',
			abstract: true,
			cache:false,
			templateUrl: 'templates/site-menu.html'//,
			//controller: 'AppCtrl'
		})	
		.state('app.top', {
			url: '/top',
			cache:false,
			views: {
				'siteMenuContent': {
				templateUrl: 'templates/top.html'
				}
			}
		})
		.state('app.login', {
			url: '/login',
			cache:false,
			views: {
				'siteMenuContent': {
				templateUrl: 'templates/login.html'
				}
			}
		})
		.state('app.slide-menu', {
			url: '/slide-menu',
			cache:false,
			views: {
				'siteMenuContent': {
				templateUrl: 'templates/slide-menu.html'
				}
			}
		})
		.state('app.cart', {
			url: '/cart',
			cache:false,
			views: {
				'siteMenuContent': {
				templateUrl: 'templates/cart.html'
				}
			}
		})
		.state('app.checkout', {
			url: '/checkout',
			cache:false,
			views: {
				'siteMenuContent': {
				templateUrl: 'templates/checkout.html'
				}
			}
		})
		.state('app.checkout-success', {
			url: '/checkout-success',
			cache:false,
			views: {
				'siteMenuContent': {
				templateUrl: 'templates/checkout-success.html'
				}
			}
		});
  
	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/app/top');
});
